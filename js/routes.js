angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('cameraTabDefaultPage', {
    url: '/page2',
    templateUrl: 'templates/cameraTabDefaultPage.html',
    controller: 'cameraTabDefaultPageCtrl'
  })

  .state('eventos', {
    url: '/page18',
    templateUrl: 'templates/eventos.html',
    abstract:true
  })

  .state('registroEmpresa', {
    url: '/page1',
    templateUrl: 'templates/registroEmpresa.html',
    controller: 'registroEmpresaCtrl'
  })

  .state('iniciarSesiNEmpresa', {
    url: '/page10',
    templateUrl: 'templates/iniciarSesiNEmpresa.html',
    controller: 'iniciarSesiNEmpresaCtrl'
  })

  .state('iniciarSesiN', {
    url: '/page9',
    templateUrl: 'templates/iniciarSesiN.html',
    controller: 'iniciarSesiNCtrl'
  })

  .state('registro', {
    url: '/page8',
    templateUrl: 'templates/registro.html',
    controller: 'registroCtrl'
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='eventos.nombreDelEvento'
      2) Using $state.go programatically:
        $state.go('eventos.nombreDelEvento');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page18/tab2/page11
      /page18/tab1/page11
  */
  .state('eventos.nombreDelEvento', {
    url: '/page11',
    views: {
      'tab2': {
        templateUrl: 'templates/nombreDelEvento.html',
        controller: 'nombreDelEventoCtrl'
      },
      'tab1': {
        templateUrl: 'templates/nombreDelEvento.html',
        controller: 'nombreDelEventoCtrl'
      }
    }
  })

  .state('mapsExample', {
    url: '/page12',
    templateUrl: 'templates/mapsExample.html',
    controller: 'mapsExampleCtrl'
  })

  .state('eventos.estaSemana', {
    url: '/page13',
    views: {
      'tab2': {
        templateUrl: 'templates/estaSemana.html',
        controller: 'estaSemanaCtrl'
      }
    }
  })

  .state('favoritos', {
    url: '/page25',
    templateUrl: 'templates/favoritos.html',
    controller: 'favoritosCtrl'
  })

  .state('eventos.prXimosEventos', {
    url: '/page14',
    views: {
      'tab1': {
        templateUrl: 'templates/prXimosEventos.html',
        controller: 'prXimosEventosCtrl'
      }
    }
  })

  .state('eventos.eventosPasados', {
    url: '/page15',
    views: {
      'tab3': {
        templateUrl: 'templates/eventosPasados.html',
        controller: 'eventosPasadosCtrl'
      }
    }
  })

  .state('registroDeUsuario', {
    url: '/page16',
    templateUrl: 'templates/registroDeUsuario.html',
    controller: 'registroDeUsuarioCtrl'
  })

  .state('crearEvento', {
    url: '/page18',
    templateUrl: 'templates/crearEvento.html',
    controller: 'crearEventoCtrl'
  })

  .state('perfil', {
    url: '/page17',
    templateUrl: 'templates/perfil.html',
    controller: 'perfilCtrl'
  })

  .state('altaDeEmpleados', {
    url: '/page21',
    templateUrl: 'templates/altaDeEmpleados.html',
    controller: 'altaDeEmpleadosCtrl'
  })

  .state('avances', {
    url: '/page23',
    templateUrl: 'templates/avances.html',
    controller: 'avancesCtrl'
  })

  .state('page19', {
    url: '/page19',
    templateUrl: 'templates/page19.html',
    controller: 'page19Ctrl'
  })

  .state('listaDeEmpleados', {
    url: '/page24',
    templateUrl: 'templates/listaDeEmpleados.html',
    controller: 'listaDeEmpleadosCtrl'
  })

  .state('compartirEvento', {
    url: '/page26',
    templateUrl: 'templates/compartirEvento.html',
    controller: 'compartirEventoCtrl'
  })

  .state('blogPost', {
    url: '/page27',
    templateUrl: 'templates/blogPost.html',
    controller: 'blogPostCtrl'
  })

  .state('historial', {
    url: '/page20',
    templateUrl: 'templates/historial.html',
    controller: 'historialCtrl'
  })

  .state('blog', {
    url: '/page22',
    templateUrl: 'templates/blog.html',
    controller: 'blogCtrl'
  })

$urlRouterProvider.otherwise('/page9')

  

});